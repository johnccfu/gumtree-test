import React, {Component} from 'react';
import ProductInfoPane from '../ProductInfoPane/productInfoPane';
import Pager from '../Pager/pager';
import {getNextPrevPositions} from '../../libs/PagerHelper';

export default
class Accordion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPosition : 0,        // current content slide
            expanded: true,             // accordion is expanded or not
            classname: 'heading open'   // default css class for open state
        };
        this.toggleAccordion = this.toggleAccordion.bind(this);
    }

    componentDidUpdate(prevProps, prevState) {
        // focus management for accessibility.
        // focus on the content pane when
        //     - accordion expanded from the collapsed state.
        //     - current position has been updated.

        if (prevState.expanded !== this.state.expanded) {
            this.focusOnProductInfoPane(2500);
        }
        else if (prevState.currentPosition !== this.state.currentPosition) {
            this.focusOnProductInfoPane(0);
        }
    }

    /**
     * Focus on the productInfoPane.
     * @param delay - delay in milliseconds
     */
    focusOnProductInfoPane = (delay) => {
        setTimeout(function() {
            let $contentPane = document.querySelector('.productInfoPane');
            if (!!$contentPane) {
                $contentPane.focus();
            }
        }, delay);
    };

    /**
     * Next button click event handler
     */
    handleNextClick = () => {
        let nextPos = getNextPrevPositions(this.props.content, this.state.currentPosition).nextPos;
        if (nextPos !== undefined) {
            this.setState({currentPosition: nextPos});
        }
    };

    /**
     * Previous button click event handler.
     */
    handlePrevClick = () => {
        let prevPos = getNextPrevPositions(this.props.content, this.state.currentPosition).prevPos;
        if (prevPos !== undefined) {
            this.setState({currentPosition: prevPos});
        }
    };

    render() {
        return (
            <div className="accordion">
                <div className={this.state.classname} role="heading">
                    <a id="btnAccordionHeading" href="#" role="button" aria-expanded={this.state.expanded} aria-controls="accordionContent" onClick={this.toggleAccordion}>
                        <i className="fa fa-file fa-lg" aria-hidden="true"></i>
                        { this.props.title }
                    </a>
                </div>
                <div id="accordionContent" role="presentation" aria-labelledby="btnAccordionHeading" className="accordion_content" aria-hidden={!this.state.expanded}>
                    <ProductInfoPane content={this.props.content} currentPos={this.state.currentPosition} />
                    <Pager pages={this.props.content} currentPos={this.state.currentPosition} onNextClick={this.handleNextClick} onPrevClick={this.handlePrevClick} />
                </div>
            </div>
        )
    }

    // Event handler to toggle the accordion open/close states
    toggleAccordion() {
        this.setState({
            expanded: !this.state.expanded,
            classname: 'heading ' + (!this.state.expanded ? 'open' : 'close')
        });
    }
}

Accordion.proptypes = {
    title: React.PropTypes.string.isRequired,
    content : React.PropTypes.arrayOf(React.PropTypes.object).isRequired
};