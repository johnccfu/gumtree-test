import React from 'react';

/**
 * This component renders out an Image
 * @param props - imagePath and imageAlt are required.
 * @constructor
 */
export const Image = (props) => {
    return (
        <div className="productImage"><img src={props.imagePath} alt={props.imageAlt} /></div>
    )
};
Image.propTypes = {
    imagePath: React.PropTypes.string.isRequired,  // image path
    imageAlt: React.PropTypes.string.isRequired    // alt text for the thumbnail
}