import React from 'react';
import {Image} from './image';
import {Description} from './description';

export default class ProductInfoPane extends React.Component {
    state = {
        thumbnail: '',
        description: ''
    };
    shouldComponentUpdate(nextProps, nextState) {
        // only update the component when the currentPos has changed
        return (
            nextProps.currentPos !== this.props.currentPos ||
            nextProps.content !== this.props.content ||
            nextState.thumbnail !== this.state.thumbnail ||
            nextState.description !== this.state.description
        );
    }

    componentDidUpdate(prevProps, prevState) {
        // update the thumbnail and description
        let thumbnail = this.props.content[this.props.currentPos].thumbnail,
            description = this.props.content[this.props.currentPos].description;

        thumbnail = !!thumbnail ? 'images/' + thumbnail : prevState.thumbnail;
        this.setState({thumbnail, description});
    }

    render() {
        if (!!this.props.content.length) {
            return null;
        }
        return (
            <div className="productInfoPane" tabIndex="-1">
                <Image imagePath={this.state.thumbnail} imageAlt="product image"/>
                <Description description={this.state.description}/>
            </div>
        )
    }
}
ProductInfoPane.propTypes = {
    content: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    currentPos: React.PropTypes.number.isRequired
};