import React, {Component} from 'react';
import axios from 'axios';
import './App.css';
import Accordion from './components/Accordion/accordion';

class App extends Component {
    state = {
        data: {
            title: '',
            content: []
        }
    };

    componentDidMount() {
        // Load the json file after the component has mounted and set the data to state
        let content = sessionStorage.getItem('ipad-mini-content');
        this.data = {
                title: 'Oops! Cannot retrieve the data at the moment',
                content: []
            };

        if (!content) {
            axios.get('data/content.json')
                .then(res => {
                    this.data = res.data;
                    sessionStorage.setItem('ipad-mini-content', JSON.stringify(this.data));
                    this.setState({ data : this.data });
                })
                .catch(function(error) {
                    // error catching
                    console.log(error);
                    this.setState({ data : this.data });
                }.bind(this))
        }
        else {
            try {
                this.data = JSON.parse(content);
            }
            catch(error) {
                // error catching
                console.log(error);
            }
            finally {
                this.setState({ data : this.data });
            }
        }

    }

    render() {
        return (
            <div className="App">
                <Accordion {...this.state.data}/>
            </div>
        );
    }
}

export default App;
