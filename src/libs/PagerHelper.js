export const getNextPrevPositions = (array, currentPos) => {
    if (array.length > 0) {
        if (currentPos === 0) {
            return {
                nextPos : 1
            }
        }
        else if (currentPos > 0) {
            let nextPos, prevPos;
            prevPos = currentPos - 1;

            if (currentPos < array.length - 1) {
                nextPos = currentPos + 1;
            }
            return {
                nextPos: nextPos,
                prevPos: prevPos
            }
        }
    }
};