import React from 'react';

/**
 * Renders the Product Description
 * @param props - description containing HTML
 * @constructor
 */
export const Description = (props) => (
    <div className="productDescription" dangerouslySetInnerHTML={{__html:props.description}}></div>
);

Description.propTypes = {
    description: React.PropTypes.string.isRequired
};
