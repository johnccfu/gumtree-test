import React from 'react';

export const PrevButton = (props) => (
    <a href="#" onClick={props.onClick} className="pull-left button-prev" role="button"
       disabled={!props.titleLabel} aria-hidden={!props.titleLabel} tabIndex={!props.titleLabel ? '-1' : '0'}>
        <span className="sr-only">Previous - </span><span className="titleLabel">{ props.titleLabel }</span>
    </a>
)

PrevButton.propTypes = {
    onClick: React.PropTypes.func.isRequired
}