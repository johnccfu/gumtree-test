import React from 'react';
import {NextButton, PrevButton} from '.'
import {getNextPrevPositions} from '../../libs/PagerHelper';

export default
class Pager extends React.Component {
    state = {
        nextTitle: '',
        prevTitle: ''
    };

    // only update when the props or state has changed.
    shouldComponentUpdate(nextProps, nextState) {
        return (
            nextProps.pages.length !== this.props.pages.length
            || nextState.nextTitle !== this.state.nextTitle
            || nextState.prevTitle !== this.state.prevTitle
            || nextProps.currentPos !== this.props.currentPos
        );
    }

    componentDidUpdate() {
        this.setTitles();
    }

    // sets the next and prev button titles based on the current position of the pager
    setTitles = () => {
        let positions = getNextPrevPositions(this.props.pages, this.props.currentPos),
            nextTitle = positions.nextPos !== undefined ? this.props.pages[positions.nextPos].title : '',
            prevTitle = positions.prevPos !== undefined ? this.props.pages[positions.prevPos].title : '';

        this.setState({
            prevTitle: prevTitle,
            nextTitle: nextTitle
        });
    };

    render() {
        if (!!this.props.pages.length) {
            return null;
        }
        return (
            <div className="pager">
                <PrevButton titleLabel={this.state.prevTitle} onClick={this.props.onPrevClick}/>
                <NextButton titleLabel={this.state.nextTitle} onClick={this.props.onNextClick}/>
            </div>
        )
    }
}
Pager.PropTypes = {
    pages: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    defaultPos: React.PropTypes.number.isRequired
};