import React from 'react';

export const NextButton = (props) => (
    <a href="#" onClick={props.onClick} className="pull-right button-next" role="button"
       disabled={!props.titleLabel} aria-hidden={!props.titleLabel} tabIndex={!props.titleLabel ? '-1' : '0'}>
        <span className="sr-only">Next - </span><span className="titleLabel">{ props.titleLabel }</span>
    </a>
)
NextButton.propTypes = {
    onClick: React.PropTypes.func.isRequired
};