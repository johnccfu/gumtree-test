This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

#Installing the project
npm install

#Running the project
**npm start** - This will run the project in development mode

**npm run build** - This will build the project for distribution.  Includes the compilation and minification of all js files and scss files in the project.

##What's included?

####Comonents structure
The project starts from the **App.js** which is the starting point of making use of the rest of the components.
The structure of the components in heirarchy are as below:

    - App
        - Accordion
        - ProjectInfoPane
            - Description
            - Image
        - Pager
            - NextButton
            - PrevButton

####Paging logic           
There is also a **_libs_** folder that contains the logic behind the paging.

####SASS 
This project also uses SASS for styling purposes.  Each of the components have their own scss file located under the related component's folder for grouping purposes, making it easier to find the relevant styles.

####Accessibility (A11y)
A11y was considered when building the components and have been incorporated where applicable.  eg. The use of aria-expanded in the accordion to indicate that whether the accordion is expanded or not.  Also focus management has been put in, so that the screen reader reads out the main content as soon as the next/prev button is pressed.
 
 
